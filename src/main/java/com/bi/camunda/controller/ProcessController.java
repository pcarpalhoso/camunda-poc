package com.bi.camunda.controller;

import com.bi.camunda.model.Employee;
import com.bi.camunda.service.OnboardingService;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Controller
public class ProcessController {

    @Autowired
    private OnboardingService onboardingService;

    private final static Logger LOGGER = Logger.getLogger("CONTROLLER-REQUEST");
    private String instanceId;
    private Employee employee;

    private String getViewName() {
        Task task = onboardingService.retrieveLastTask();

        return task.getFormKey();
    }

    @PostMapping("/startProcess")
    public String startProcess(Model model){


        employee = new Employee();
        model.addAttribute("employee", employee);

        Map<String, Object> variables = new HashMap<>();
        variables.put("employee", employee);

        instanceId = onboardingService.startProcess("onboarding", variables);

        return getViewName();
    }

    @GetMapping("/employee")
    public String employeeForm(){

        Execution execution = onboardingService.getExecution(instanceId);
        onboardingService.setEmployee(execution.getId(), employee);
        return "employee";
    }

    @PostMapping("/employee")
    public String employeeSubmit(@ModelAttribute Employee employee){

        String taskId = onboardingService.retrieveLastTaskId();

        Execution execution = onboardingService.getExecution(instanceId);
        onboardingService.setEmployee(execution.getId(), employee);
        onboardingService.completeTask(taskId);

        return getViewName();
    }

    @PostMapping("/result")
    public void resultSubmit() {

        Execution execution = onboardingService.getExecution(instanceId);
        Employee employee = onboardingService.getEmployee(execution.getId());

        LOGGER.info("The employee is: ".concat(employee.getFirstName()));

    }
}
