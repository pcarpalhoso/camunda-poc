package com.bi.camunda.service;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class OnboardingDelegate implements JavaDelegate{

    private final static Logger LOGGER = Logger.getLogger("ONBOARDING-REQUEST");

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LOGGER.info("Spring Bean OnboardingDelegate invoked !!!!!");
    }
}
