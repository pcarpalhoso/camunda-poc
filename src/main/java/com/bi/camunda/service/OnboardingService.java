package com.bi.camunda.service;

import com.bi.camunda.model.Employee;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class OnboardingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OnboardingService.class);

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    public String startProcess(String processName) {
        return startProcess(processName, null);
    }

    public String startProcess(String processName, Map<String, Object> variables) {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey (processName, variables);
        return processInstance.getId();
    }

    public void setEmployee(String executionId, Employee employee) {
        runtimeService.setVariable(executionId,"employee", employee);
    }

    public Employee getEmployee(String executionId) {
        Employee employee = new Employee();

        employee = (Employee) runtimeService.getVariable(executionId, "employee");
        return employee;
    }

    public List<Task> retrieveTasks() {
        return taskService.createTaskQuery()
                .active()
                .initializeFormKeys()
                .list();

    }

    public Task retrieveLastTask() {
        return taskService.createTaskQuery()
                .active()
                .initializeFormKeys()
                .singleResult();
    }

    public String retrieveLastTaskId() {
        return retrieveLastTask().getId();
    }

    public Execution getExecution(String processInstanceId) {
        return runtimeService.createExecutionQuery()
                .processInstanceId(processInstanceId)
                .list()
                .get(0);
    }

    public void completeTask(String taskId) {
        taskService.complete(taskId);
    }
}
